Hiking Band App

Main features:

- An application on Lilygo Smart watch to record a hiking session with the step counter,
hiking time, gps coordinates and hiking distances.
- A python scripts running on Raspberry BI to calculate the calorie burnt based on step counter, hiking time, user's weight and travel distances
- All of the information are displayed on the Unicorn HD matri

## Installation Dependencies
Below dependencies need to be installed

|Dependencies           	 | Home Page                                               |
|----------------------------|---------------------------------------------------------|
| Arduino IDE           	 | <https://www.arduino.cc/>                               |
| Python                     | <https://www.python.org/downloads/>                     |
| LiLyGo TWatch Library 	 | <https://github.com/Xinyuan-LilyGO/TTGO_TWatch_Library> |
| Unicorn HD Library         | <https://github.com/pimoroni/unicorn-hat-hd>            |
| Python Image Learning fork | <https://github.com/python-pillow>                      |
| TeraTerm(Optional)     	 | <https://ttssh2.osdn.jp/index.html.en>                  |

## Usage

When we press the power button to turn on the watch (for at lest 2 seconds), it starts the hiking session.

When the hiking session ends, users just need to press the power button again(for at lest 2 seconds)  to turn off the watch and stop the hiking session.

Run the below scripts on Raspberry Pi to get the calorie burnt based on step counter, hiking time, user's weight and travel distances on Unicorn LED

```python
python calories_and_display_RPi.py
```


## Display information in the Unicorn LED

Using stimulation software:
```python
from unicorn_hat_sim import unicornhathd as unicorn
```

Using real hardware
```python
import unicornhathd as unicorn
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
