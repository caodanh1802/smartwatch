#define LILYGO_WATCH_2020_V2
#define LILYGO_WATCH_LVGL   
#include <LilyGoWatch.h>
#include <bits/stdc++.h> 

TTGOClass *watch;
TFT_eSPI *tft;
BMA *sensor;
bool irq = false;

TinyGPSPlus *gps = nullptr;

TFT_eSprite *eSpLoaction = nullptr;
TFT_eSprite *eSpTime = nullptr;
TFT_eSprite *eSpSpeed = nullptr;

uint32_t last = 0;
uint32_t updateTimeout = 0;
uint32_t stepCount = 0;

char buf[128];
bool rtcIrq = false;
bool isOriginal = true;

static double lat1 = 0;
static double long1 = 0;

static double travelDistance = 0;

static void event_handler(lv_obj_t *obj, lv_event_t event)
{
    if (event == LV_EVENT_CLICKED) {
        Serial.printf("StartSession\n");
    } else if (event == LV_EVENT_VALUE_CHANGED) {
        Serial.printf("StopSession\n");
    }
}

void setup()
{
    Serial.begin(115200);

    // Get TTGOClass instance
    watch = TTGOClass::getWatch();

    // Initialize the hardware, the BMA423 sensor has been initialized internally
    watch->begin();

    // Turn on the backlight
    watch->openBL();

    // Setup for timer
    pinMode(RTC_INT_PIN, INPUT_PULLUP);
    attachInterrupt(RTC_INT_PIN, [] {
        rtcIrq = 1;
    }, FALLING);

    //Receive objects for easy writing
    tft = watch->tft;
    sensor = watch->bma;

    tft->fillScreen(TFT_BLACK);
    tft->setTextFont(2);
    tft->println("Begin Session");

    watch->rtc->disableAlarm();
    watch->rtc->setDateTime(2022, 3, 22, 00, 00, 00);
    watch->rtc->setAlarmByMinutes(1);
    watch->rtc->enableAlarm();

    //Open gps power
    watch->trunOnGPS();

    watch->gps_begin();

    gps = watch->gps;

    // Display on the screen, latitude and longitude, number of satellites, and date and time
    eSpLoaction   = new TFT_eSprite(tft); // Sprite object for eSpLoaction
    eSpTime   = new TFT_eSprite(tft); // Sprite object for eSpTime

    eSpLoaction->createSprite(240, 48);
    eSpLoaction->setTextFont(2);

    eSpTime->createSprite(240, 48);
    eSpTime->setTextFont(2);

    last = millis();

    // Accel parameter structure
    Acfg cfg;
    /*!
        Output data rate in Hz, Optional parameters:
    */
    cfg.odr = BMA4_OUTPUT_DATA_RATE_100HZ;
    /*!
        G-range, Optional parameters:
    */
    cfg.range = BMA4_ACCEL_RANGE_2G;
    /*!
        Bandwidth parameter, determines filter configuration, Optional parameters:
    */
    cfg.bandwidth = BMA4_ACCEL_NORMAL_AVG4;

    /*! Filter performance mode , Optional parameters:
        - BMA4_CIC_AVG_MODE
        - BMA4_CONTINUOUS_MODE
    */
    cfg.perf_mode = BMA4_CONTINUOUS_MODE;

    // Configure the BMA423 accelerometer
    sensor->accelConfig(cfg);

    // Enable BMA423 accelerometer
    // Warning : Need to use steps, you must first enable the accelerometer
    sensor->enableAccel();

    pinMode(BMA423_INT1, INPUT);
    attachInterrupt(BMA423_INT1, [] {
        // Set interrupt to set irq value to 1
        irq = 1;
    }, RISING); //It must be a rising edge

    // Enable BMA423 step count feature
    sensor->enableFeature(BMA423_STEP_CNTR, true);

    // Reset steps
    sensor->resetStepCounter();

    // Turn on step interrupt
    sensor->enableStepCountInterrupt();

    // Some display settings
    tft->setTextColor(random(0xFFFF));
    // tft->drawString("Session Begin", 3, 50, 4);
    tft->setTextFont(2);
    tft->setTextColor(TFT_WHITE, TFT_BLACK);
    Serial.println(F("Session Begin: "));
}

void loop()
{  
  watch->tft->drawString("Session Begin", 3, 50, 4); 
   snprintf(buf, sizeof(buf), "%s", watch->rtc->formatDateTime());
    // watch->tft->drawString(buf, 2, 90, 7);
    tft->setCursor(10, 70);
    tft->print(buf);
    Serial.print(F("Timer"));
    Serial.println(buf);
    Serial.print(F("Travel Distance"));
    Serial.println(travelDistance);
    Serial.print(F("Step Count= "));
    Serial.println(stepCount);
    watch->tft->drawString("Travel Distance", 3, 95, 4);
      tft->setCursor(10, 130);
    tft->print(travelDistance);
    if (rtcIrq) {
        rtcIrq = 0;
        detachInterrupt(RTC_INT_PIN);
        watch->rtc->resetAlarm();
        int i = 3;
        while (i--) {
            watch->tft->fillScreen(TFT_RED);
            watch->tft->setTextColor(TFT_WHITE, TFT_RED);
            watch->tft->drawString("Interrupt", 60, 118, 4);
            delay(500);
            watch->tft->fillScreen(TFT_BLACK);
            watch->tft->setTextColor(TFT_WHITE, TFT_BLACK);
            // watch->tft->drawString("RTC Alarm", 60, 118, 4);
            delay(500);
        }
    }
    delay(1000);
  watch->gpsHandler();
      if (gps->location.isUpdated()) {
        updateTimeout = millis();

        if (isOriginal){
           long double lat1 = gps->location.lat();
           long double long1 = gps->location.lng();
           isOriginal = false;
        }
        else {
           double lat2 = gps->location.lat();
           double long2 = gps->location.lng();
        }        
        eSpLoaction->fillSprite(TFT_BLACK);
        eSpLoaction->setTextColor(TFT_GREEN, TFT_BLACK);
        eSpLoaction->setCursor(0, 0);
        eSpLoaction->print("LOCATION ");
        eSpLoaction->print("Fix Age=");
        eSpLoaction->println(gps->location.age());
        eSpLoaction->print("Lat= ");
        eSpLoaction->print(gps->location.lat(), 6);
        eSpLoaction->print(" Long= ");
        eSpLoaction->print(gps->location.lng(), 6);
        eSpLoaction->pushSprite(0, 0);

    } else {
        if (millis() - updateTimeout > 3000) {
            updateTimeout = millis();

            eSpLoaction->fillSprite(TFT_BLACK);
            eSpLoaction->setTextColor(TFT_GREEN);
            eSpLoaction->setCursor(0, 0);
            eSpLoaction->print("LOCATION ");
            eSpLoaction->setTextColor(TFT_RED);
            eSpLoaction->print("INVAILD");
            eSpLoaction->pushSprite(0, 0);
        }
    }

    if (gps->time.isUpdated()) {
        eSpTime->setTextColor(TFT_GREEN, TFT_BLACK);
        eSpTime->fillSprite(TFT_BLACK);
        // eSpTime->setCursor(0, 0);
        /* 
        eSpTime->print("TIME Fix Age= ");
        eSpTime->println(gps->time.age());
        eSpTime->print("Hour=");
        eSpTime->print(gps->time.hour());
        eSpTime->print(" Minute=");
        eSpTime->print(gps->time.minute());
        eSpTime->print(" Second=");
        eSpTime->println(gps->time.second());
        eSpTime->pushSprite(0, 49 * 2);
        */
    }

    if (gps->speed.isUpdated()) {
    }

    if (millis() - last > 5000) {
        Serial.println();
        if (gps->location.isValid()) {
            // static const double LONDON_LAT = 51.508131, LONDON_LON = -0.128002;
            /* 
            double distanceToLondon =
                TinyGPSPlus::distanceBetween(
                    gps->location.lat(),
                    gps->location.lng(),
                    LONDON_LAT,
                    LONDON_LON);
            double courseToLondon =
                TinyGPSPlus::courseTo(
                    gps->location.lat(),
                    gps->location.lng(),
                    LONDON_LAT,
                    LONDON_LON);
            */

            travelDistance = TinyGPSPlus::distanceBetween(
                    gps->location.lat(),
                    gps->location.lng(),
                    lat1,
                    long1);

            travelDistance = travelDistance / 1000;
            
            /*
            Serial.print(F("LONDON     Distance="));
            Serial.print(distanceToLondon / 1000, 6);
            Serial.print(F(" km Course-to="));
            Serial.print(courseToLondon, 6);
            Serial.print(F(" degrees ["));
            Serial.print(TinyGPSPlus::cardinal(courseToLondon));
            Serial.println(F("]"));
            */
        }

        if (gps->charsProcessed() < 10) {
            Serial.println(F("WARNING: No GPS data.  Check wiring."));
            tft->fillScreen(TFT_BLACK);
            tft->setTextColor(TFT_RED, TFT_BLACK);
            tft->println("ERROR: No GPS data.  Check wiring.");
            while (1);
        }

        last = millis();
        Serial.println();
    }
    if (irq) {
        irq = 0;
        bool  rlst;
        do {
            // Read the BMA423 interrupt status,
            // need to wait for it to return to true before continuing
            rlst =  sensor->readInterrupt();
        } while (!rlst);

        // Check if it is a step interrupt
        if (sensor->isStepCounter()) {
            // Get step data from register
            stepCount = sensor->getCounter();
            tft->setTextColor(TFT_GREEN, TFT_BLACK);
            tft->setCursor(45, 180);
            tft->print("StepCount: ");
            tft->print(stepCount);
            // Serial.print(F("Step Count= "));
            // Serial.print(step);
        }
    }
    delay(20);
}
