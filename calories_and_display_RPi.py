import colorsys
import time
from sys import exit
from PIL import Image, ImageDraw, ImageFont, ImageColor

# When using with the actual LED array, uncomment the try-except and index unicorn hat sim
# try:
    # import unicornhathd as unicorn
    # print("unicorn hat hd detected")
# except ImportError:
from unicorn_hat_sim import unicornhathd as unicorn


def caloriecalc(weight, time):
    MET_walking = 3.3       # https://www.omicsonline.org/articles-images/2157-7595-6-220-t003.html
    calories = time * 60 * MET_walking * 3.5 * weight / 200
    return calories


def main():

    weight = 75              # A somewhat average human weight

    path = 'data-from-watch.log'
    trackdata = open(path, 'r')
    row = trackdata.readlines()
    row = row[-3:]
    row[0] = row[0].strip()
    timer = row[0].strip('Timer')
    dist = row[1].strip('Travel Distance')
    steps = row[2].strip('Step Count= ')
    steps = int(steps)
    timer = timer.split(":")
    timer[0], timer[1], timer[2] = int(timer[0]), int(timer[1]), int(timer[2])
    print(timer)
    dist = float(dist)
    hours = float(timer[0] + timer[1]/60 + timer[2]/3600)

    calories = float(caloriecalc(weight, hours))
    print(calories)

    lines = ["Steps taken: {:d} — Time elapsed: {:>4d} h {:>4d} min "
             "{:>4d} sec — Distance travelled: {:.2f} km — Calories burned: {:.2f} cal"
            .format(steps, timer[0], timer[1], timer[2], dist, calories)]
             
    colour = (255,255,255) # Text colour set to white (R/G/B = 255)
    FONT = ('/usr/share/fonts/truetype/freefont/FreeMonoBold.ttf',
            12)

    unicorn.rotation(0)
    unicorn.brightness(1)

    width, height = unicorn.get_shape()
    text_x = width
    text_y = 2
    font_file, font_size = FONT
    font = ImageFont.truetype(font_file, font_size)
    text_width, text_height = width, height

    try:
        for line in lines:
            w, h = font.getsize(line)
            text_width += w + width
            text_height = max(text_height, h)

        text_width += width + text_x + 1

        image = Image.new('RGB', (text_width, max(16, text_height)),
                (0, 0, 0))
        draw = ImageDraw.Draw(image)

        offset_left = 0

        for index, line in enumerate(lines):
            draw.text((text_x + offset_left, text_y), line,
                       colour, font=font)

            offset_left += font.getsize(line)[0] + width

        for scroll in range(text_width - width):
            for x in range(width):
                for y in range(height):
                    pixel = image.getpixel((x + scroll, y))
                    r, g, b = [int(n) for n in pixel]
                    unicorn.set_pixel(width - 1 - x, y, r, g, b)

            unicorn.show()
            time.sleep(0.07)

    except KeyboardInterrupt:
        unicorn.off()

    finally:
        unicorn.off()


main()
